<?php

namespace Database\Seeders;

use App\Models\Event;
use Illuminate\Database\Seeder;

class EventSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= 100; $i++) {
            Event::create(['label' => "Event#" . $i . '-' . time(), "code" => "Code#" . $i . '-' . time()]);
        }
    }
}
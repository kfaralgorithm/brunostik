<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateV2PronosticsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('v2_pronostics', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->time('hours');
            $table->integer('status')->nullable(true);
            $table->string('result')->nullable(true);
            $table->string('prediction');

            $table->string('team_one');
            $table->string('team_two');
            $table->string('sport');
            $table->string('championat');
            $table->string('event');
            $table->softDeletes();
            $table->timestamps();


            $table->uuid('bet_id')->index();
            $table->foreign('bet_id')->references('id')->on('bets');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('v2_pronostics');
    }
}

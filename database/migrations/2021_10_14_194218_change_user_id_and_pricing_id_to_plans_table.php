<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeUserIdAndPricingIdToPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('plans', function (Blueprint $table) {
            $table->dropColumn('user_id');
        });
        Schema::table('plans', function (Blueprint $table) {
            $table->uuid('user_id')->index();
            $table->foreign('user_id')->references('id')->on('users');
        });


        Schema::table('plans', function (Blueprint $table) {
            $table->dropColumn('pricing_id');
        });
        Schema::table('plans', function (Blueprint $table) {
            $table->uuid('pricing_id')->index();
            $table->foreign('pricing_id')->references('id')->on('pricings');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pricings', function (Blueprint $table) {
            //
        });
    }
}

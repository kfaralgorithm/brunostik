<?php

namespace App\Models\V2;

use App\Models\Bet;
use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Pronostic extends Model
{
    use HasFactory, Uuids;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'hours',
        'status',
        'cote',
        'result',
        'prediction',
        'team_one',
        'team_two',
        'sport',
        'championat',
        'event',
        'bet_id',
    ];



    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = "v2_pronostics";



    public function bet()
    {
        return $this->belongsTo(Bet::class, 'bet_id');
    }
}

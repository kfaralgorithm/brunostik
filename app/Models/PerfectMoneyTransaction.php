<?php

namespace App\Models;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class PerfectMoneyTransaction extends Model
{
    use HasFactory, Uuids, SoftDeletes;



    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'perfect_money_transactions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_ref',
        'pricing_pk',
        'status',
        'request_body',
    ];
}

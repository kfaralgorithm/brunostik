<?php

namespace App\Interfaces\Secured;

use Illuminate\Http\Request;
use App\Http\Requests\ChampionatCreateOrUpdateRequest;

interface ChampionatRepositoryInterface
{
    /**
     * Get all championats
     *
     * @method  GET api/championats
     * @access  public
     */
    public function getAll(Request $request);

    /**
     * Get Championat By ID
     *
     * @param   \Ramsey\Uuid\Uuid      $id
     *
     * @access  public
     */
    public function getById($id);

    /**
     * Create | Update championat
     *
     * @param   \App\Http\Requests\ChampionatCreateOrUpdateRequest    $request
     * @param   \Ramsey\Uuid\Uuid                           $id
     *
     * @access  public
     */
    public function createOrUpdate(ChampionatCreateOrUpdateRequest $request, $id = null);

    /**
     * Delete championat
     *
     * @param   \Ramsey\Uuid\Uuid     $id
     *
     * @access  public
     */
    public function delete($id);
}

<?php

namespace App\Interfaces\Secured;

use Illuminate\Http\Request;
use App\Http\Requests\SportCreateOrUpdateRequest;

interface SportRepositoryInterface
{
    /**
     * Get all sports
     *
     * @method  GET api/sports
     * @access  public
     */
    public function getAll(Request $request);

    /**
     * Get Sport By ID
     *
     * @param   \Ramsey\Uuid\Uuid      $id
     *
     * @method  GET api/sports/{id}
     * @access  public
     */
    public function getById($id);

    /**
     * Create | Update sport
     *
     * @param   \App\Http\Requests\SportCreateOrUpdateRequest    $request
     * @param   \Ramsey\Uuid\Uuid                           $id
     *
     * @method  POST    api/sports       For Create
     * @method  PUT     api/sports/{id}  For Update
     * @access  public
     */
    public function createOrUpdate(SportCreateOrUpdateRequest $request, $id = null);

    /**
     * Delete sport
     *
     * @param   \Ramsey\Uuid\Uuid     $id
     *
     * @method  DELETE  api/sports/{id}
     * @access  public
     */
    public function delete($id);
}

<?php

namespace App\Interfaces\ThirdParty;

use App\Http\Requests\PerfectMoneyFailedRequest;
use App\Http\Requests\PerfectMoneySuccessRequest;

interface PerfectMoneyRepositoryInterface
{


    /**
     *
     * @param   \App\Http\Requests\PerfectMoneySuccessRequest    $request
     *
     * @access  public
     */
    public function success(PerfectMoneySuccessRequest $request);

    /**
     *
     * @param   \App\Http\Requests\PerfectMoneyFailedRequest    $request
     *
     * @access  public
     */
    public function failed(PerfectMoneyFailedRequest $request);
}

<?php

namespace App\Interfaces\Opened;

use Illuminate\Http\Request;
use App\Http\Requests\BetCreateOrUpdateRequest;

interface BetRepositoryInterface
{
    /**
     * Get all bets
     *
     * @method  GET api/bets
     * @access  public
     */
    public function getAll(Request $request);

    /**
     * Get Bet By Date
     *
     * @param  \Illuminate\Http\Request    $request
     * @param   Date     $for
     *
     * @method  GET api/bets/{for}
     * @access  public
     */
    public function getByFor(Request $request, $for);
}

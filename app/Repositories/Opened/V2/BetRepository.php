<?php

namespace App\Repositories\Opened\V2;

use Carbon\Carbon;
use App\Models\Bet;
use App\Models\Plan;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use App\Interfaces\Opened\V2\BetRepositoryInterface;
use App\Http\Resources\V2\BetWithPronosticCollection;

class BetRepository implements BetRepositoryInterface
{

    /**
     * Get Bet By Date
     *
     * @param  \Illuminate\Http\Request    $request
     * @param   Date     $for
     *
     * @method  GET api/bets/{for}
     * @access  public
     */
    public function getByFor(Request $request, $for)
    {
        try {
            $user = auth()->user();

            $plan = Plan::where('user_id', $user->id)->where('start', '<=', Carbon::now())->where('end', '>=', Carbon::now())->first();


            $now = Carbon::now()->hour(0)->minute(0)->second(0);
            $cfor = Carbon::createFromFormat(date_format_db(), $for)->hour(23)->minute(59)->second(59);

            if ($now->lte($cfor) && !$plan) {
                return ['message' => "All Bets", 'data' => null, 'statusCode' => 402];
            }



            $bets = Bet::where('for', $for)->whereHas('v2_pronostics')->orderBy('for', 'desc');


            /*
             * Filtring on status bet table field
             * status is the http request query key associated at status
             */

            //dd($request->status);
            if ($request->has('status') && in_array($request->status, [-1, 0, 1, 2])) {
                $bets = $request->status == 2 ? $bets->whereNull('status') : $bets->where('status', $request->status);
            }
            /*
             * End filtring on status
             */

            $bets = $bets->paginate(25)->appends(Arr::except($request->query(), 'page'));
            return ['message' => "All Bets for " . $for, 'data' => new BetWithPronosticCollection($bets), 'statusCode' => 200];
        } catch (\Exception $e) {
            return ['message' => $e->getMessage(), 'data' => null, 'statusCode' => 500, 'error' => true];
        }
    }
}

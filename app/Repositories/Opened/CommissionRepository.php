<?php

namespace App\Repositories\Opened;

use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use App\Http\Resources\CommissionOpenResource;
use App\Http\Resources\CommissionOpenCollection;
use App\Interfaces\Opened\CommissionRepositoryInterface;

class CommissionRepository implements CommissionRepositoryInterface
{

    /**
     * Get all commissions associate to user connected
     *
     * @param \Illuminate\Http\Request $request
     * @access  public
     */
    public function getAll(Request $request)
    {

        // PENDIND = 0
        // DONE = 1
        // CANCELED = -1

        try {
            $commissions = null;

            if ($request->filter && $request->filter == "d") {
                $commissions = auth()->user()->commissions()->where('status', 1)->paginate(25)->appends(Arr::except($request->query(), 'page'));
            } else if ($request->filter && $request->filter == "p") {
                $commissions = auth()->user()->commissions()->where('status', 0)->paginate(25)->appends(Arr::except($request->query(), 'page'));
            } else if ($request->filter && $request->filter == "f") {
                $commissions = auth()->user()->commissions()->where('status', -1)->paginate(25)->appends(Arr::except($request->query(), 'page'));
            } else {
                $commissions = auth()->user()->commissions()->paginate(25)->appends(Arr::except($request->query(), 'page'));
            }

            return ['message' => "All Commissions", 'data' => new CommissionOpenCollection($commissions), 'statusCode' => 200];
        } catch (\Exception $e) {
            return ['message' => $e->getMessage(), 'data' => null, 'statusCode' => 500, 'error' => true];
        }
    }


    /**
     * Get Commission By ID
     *
     * @param   \Ramsey\Uuid\Uuid      $id
     *
     * @access  public
     */
    public function getById($id)
    {
        try {
            $commission = auth()->user()->commissions()->where('id', $id)->first();

            // Check the commission
            if (!$commission) return ['message' => "No commission with ID $id", 'data' => null, 'statusCode' => 404, 'error' => true];

            return ['message' => "Commission Detail", 'data' => new CommissionOpenResource($commission), 'statusCode' => 200];
        } catch (\Exception $e) {
            return ['message' => $e->getMessage(), 'data' => null, 'statusCode' => 500, 'error' => true];
        }
    }
}

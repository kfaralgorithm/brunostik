<?php

namespace App\Repositories\Opened;

use Carbon\Carbon;
use App\Models\Plan;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use App\Http\Resources\PlanResource;
use App\Http\Resources\PlanCollection;
use App\Interfaces\Opened\PlanRepositoryInterface;

class PlanRepository implements PlanRepositoryInterface
{

    /**
     * Get all plans associate to user connected 
     *
     * @param \Illuminate\Http\Request $request
     * @access  public
     */
    public function getAll(Request $request)
    {
        // DONE = 1
        // END = 0
        // NOT_END_BUT_NEW_PLAN_DONE = -1 (overloaded_by is not null if status == -1)

        try {
            $plans = Plan::where('user_id', auth()->user()->id)->paginate(25)->appends(Arr::except($request->query(), 'page'));

            return ['message' => "All Plans", 'data' => new PlanCollection($plans), 'statusCode' => 200];
        } catch (\Exception $e) {
            return ['message' => $e->getMessage(), 'data' => null, 'statusCode' => 500, 'error' => true];
        }
    }


    /**
     * Get Plan By ID
     *
     * @param   \Ramsey\Uuid\Uuid      $id
     *
     * @access  public
     */
    public function getById($id)
    {
        try {
            $plan = Plan::where('user_id', auth()->user()->id)->where('id', $id)->first();

            // Check the plan
            if (!$plan) return ['message' => "No plan with ID $id", 'data' => null, 'statusCode' => 404, 'error' => true];

            return ['message' => "Plan Detail", 'data' => new PlanResource($plan), 'statusCode' => 200];
        } catch (\Exception $e) {
            return ['message' => $e->getMessage(), 'data' => null, 'statusCode' => 500, 'error' => true];
        }
    }


    /**
     * Get current Plan
     *
     *
     * @access  public
     */
    public function current() 
    {
        try {
            $plan = Plan::where('user_id', auth()->user()->id)->where('start', '<=', Carbon::now())->where('end', '>=', Carbon::now())->first();

            return ['message' => "Current Plan", 'data' => ['subscribed' => ($plan ? true : false), 'item' => ($plan ? new PlanResource($plan) : null)], 'statusCode' => 200];
        } catch (\Exception $e) {
            return ['message' => $e->getMessage(), 'data' => null, 'statusCode' => 100, 'error' => true];
        }
    }
}

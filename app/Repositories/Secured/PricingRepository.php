<?php

namespace App\Repositories\Secured;

use App\Models\Pricing;
use App\Traits\Response;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Resources\PricingResource;
use App\Http\Resources\PricingCollection;
use Symfony\Component\Console\Input\Input;
use App\Interfaces\Secured\PricingRepositoryInterface;
use App\Http\Requests\PricingCreateOrUpdateRequest;

class PricingRepository implements PricingRepositoryInterface
{

    /**
     * Get all pricings
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function getAll(Request $request)
    {
        try {
            $pricings = null;

            if ($request->has('sort_by') && in_array($request->sort_by, ['designation', 'recorded_at', '-designation', '-recorded_at'])) {
                $pricings = Pricing::orderBy(['designation' => 'label', 'recorded_at' =>  'created_at', '-designation' => 'label', '-recorded_at' =>  'created_at'][$request->sort_by], $request->sort_by[0] == '-' ? 'asc' : 'desc');
            } else {
                $pricings = Pricing::orderBy('created_at', 'desc');
            }


            /*
             * Filtring on label pricing table field
             * designation is the http request query key associated at label
             */
            if ($request->has('designation') && $request->designation) {
                $pricings = Pricing::where('label', 'LIKE', '%' . trim($request->designation) . '%');
            }
            /*
             * End filtring on label
             */


            /*
             * Filtring on created_at pricing table field
             * created_at is datetime
             * recorded_from <= created_at
             * created_at <= recorded_to
             * recorded_at and recorded_from is date Y-m-d
             */

            if ($request->has('recorded_from') && $request->recorded_from) {
                $pricings = $pricings == null ? Pricing::whereDate('created_at', '>=', $request->recorded_from) : $pricings->whereDate('created_at', '>=', $request->recorded_from);
            }

            if ($request->has('recorded_to') && $request->recorded_to) {
                $pricings = $pricings == null ? Pricing::whereDate('created_at', '<=', $request->recorded_to) : $pricings->whereDate('created_at', '<=', $request->recorded_to);
            }
            /*
             * End filtring on created_at
             */

            $pricings = $pricings->paginate(25)->appends(Arr::except($request->query(), 'page'));
            return ['message' => "All Pricings", 'data' => new PricingCollection($pricings), 'statusCode' => 200];
        } catch (\Exception $e) {
            return ['message' => $e->getMessage(), 'data' => null, 'statusCode' => 500, 'error' => true];
        }
    }

    /**
     * Get Pricing By ID
     *
     * @param \Ramsey\Uuid\Uuid  $id
     *
     * @return array
     */
    public function getById($id)
    {
        try {
            $pricing = Pricing::find($id);

            // Check the pricing
            if (!$pricing) return ['message' => "No pricing with ID $id", 'data' => null, 'statusCode' => 404, 'error' => true];

            return ['message' => "Pricing Detail", 'data' => new PricingResource($pricing), 'statusCode' => 200];
        } catch (\Exception $e) {
            return ['message' => $e->getMessage(), 'data' => null, 'statusCode' => 500, 'error' => true];
        }
    }

    /**
     * Create | Update pricing
     *
     * @param \App\Http\Requests\PricingCreateOrUpdateRequest $request
     * @param \Ramsey\Uuid\Uuid  $id
     *
     * @return array
     */
    public function createOrUpdate(PricingCreateOrUpdateRequest $request, $id = null)
    {

        DB::beginTransaction();
        try {

            $pricing = $id ? Pricing::find($id) : new Pricing;

            if ($id && !$pricing) return ['message' => "No pricing with ID $id", 'data' => null, 'statusCode' => 404, 'error' => true];

            if ($request->designation) {
                $pricing->label = trim($request->designation);
            }

            if ($request->percent) {
                $pricing->gift = $request->percent;
            }

            if ($request->day) {
                $pricing->day_duration = $request->day;
            }

            if ($request->month) {
                $pricing->month_duration = $request->month;
            }

            if ($request->price) {
                $pricing->price = $request->price;
            }

            if (!$id) {
                $pricing->public_key = \Ramsey\Uuid\Uuid::uuid4() . "-" . time();
            }

            $pricing->save();

            DB::commit();

            return ['message' => $id ? "Pricing updated" : "Pricing created", 'data' => $pricing, 'statusCode' => $id ? 200 : 201];
        } catch (\Exception $e) {
            DB::rollBack();
            return ['message' => $e->getMessage(), 'data' => null, 'statusCode' => 500, 'error' => true];
        }
    }

    /**
     * Delete pricing
     *
     * @param \Ramsey\Uuid\Uuid  $id
     *
     * @return array
     */
    public function delete($id)
    {
        DB::beginTransaction();
        try {
            $pricing = Pricing::find($id);

            // Check the pricing
            if (!$pricing) return ['message' => "No pricing with ID $id", 'data' => null, 'statusCode' => 404, 'error' => true];

            $pricing->label = $pricing->label . "#-#DELETE#";
            $pricing->save();

            // Delete the pricing
            $pricing->delete();

            DB::commit();
            return ['message' => "Pricing deleted", 'data' => true, 'statusCode' => 200];
        } catch (\Exception $e) {
            DB::rollBack();
            return ['message' => $e->getMessage(), 'data' => false, 'statusCode' => 500, 'error' => true];
        }
    }
}

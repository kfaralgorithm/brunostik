<?php

namespace App\Repositories\Secured;

use Carbon\Carbon;
use App\Models\Plan;
use App\Models\User;
use App\Models\Pricing;
use App\Traits\Response;
use App\Models\Commission;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use App\Models\MoovMtnTransaction;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Console\Input\Input;
use App\Http\Resources\MoovMtnTransactionResource;
use App\Http\Resources\MoovMtnTransactionCollection;
use App\Http\Requests\MoovMtnTransactionManyUpdateRequest;
use App\Interfaces\Secured\MoovMtnTransactionRepositoryInterface;

class MoovMtnTransactionRepository implements MoovMtnTransactionRepositoryInterface
{

    /**
     * Get all moovmtntransactions
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function getAll(Request $request)
    {
        try {
            $moovmtntransactions = null;

            if ($request->has('sort_by') && in_array($request->sort_by, ['type', 'recorded_at', '-type', '-recorded_at'])) {
                $moovmtntransactions = MoovMtnTransaction::orderBy(['type' => 'type', 'recorded_at' =>  'created_at', '-type' => 'type', '-recorded_at' =>  'created_at'][$request->sort_by], $request->sort_by[0] == '-' ? 'asc' : 'desc');
            } else {
                $moovmtntransactions = MoovMtnTransaction::orderBy('created_at', 'desc');
            }


            /*
             * Filtring on type moovmtntransaction table field
             * type is the http request query key associated at type
             */
            if ($request->has('type') && $request->designation) {
                $moovmtntransactions = MoovMtnTransaction::where('type', 'LIKE', '%' . trim($request->designation) . '%');
            }
            /*
             * End filtring on type
             */


            /*
             * Filtring on created_at moovmtntransaction table field
             * created_at is datetime
             * recorded_from <= created_at
             * created_at <= recorded_to
             * recorded_at and recorded_from is date Y-m-d
             */

            if ($request->has('recorded_from') && $request->recorded_from) {
                $moovmtntransactions = $moovmtntransactions == null ? MoovMtnTransaction::whereDate('created_at', '>=', $request->recorded_from) : $moovmtntransactions->whereDate('created_at', '>=', $request->recorded_from);
            }

            if ($request->has('recorded_to') && $request->recorded_to) {
                $moovmtntransactions = $moovmtntransactions == null ? MoovMtnTransaction::whereDate('created_at', '<=', $request->recorded_to) : $moovmtntransactions->whereDate('created_at', '<=', $request->recorded_to);
            }
            /*
             * End filtring on created_at
             */

            $moovmtntransactions = $moovmtntransactions->paginate(25)->appends(Arr::except($request->query(), 'page'));
            return ['message' => "All MoovMtnTransactions", 'data' => new MoovMtnTransactionCollection($moovmtntransactions), 'statusCode' => 200];
        } catch (\Exception $e) {
            return ['message' => $e->getMessage(), 'data' => null, 'statusCode' => 500, 'error' => true];
        }
    }

    /**
     * Get MoovMtnTransaction By ID
     *
     * @param \Ramsey\Uuid\Uuid  $id
     *
     * @return array
     */
    public function getById($id)
    {
        try {
            $moovmtntransaction = MoovMtnTransaction::find($id);

            // Check the moovmtntransaction
            if (!$moovmtntransaction) return ['message' => "No moovmtntransaction with ID $id", 'data' => null, 'statusCode' => 404, 'error' => true];

            return ['message' => "MoovMtnTransaction Detail", 'data' => new MoovMtnTransactionResource($moovmtntransaction), 'statusCode' => 200];
        } catch (\Exception $e) {
            return ['message' => $e->getMessage(), 'data' => null, 'statusCode' => 500, 'error' => true];
        }
    }

    /**
     * Update moovmtntransaction
     *
     * @param \App\Http\Requests\MoovMtnTransactionManyUpdateRequest $request
     *
     * @return array
     */
    public function manyUpdate(MoovMtnTransactionManyUpdateRequest $request)
    {

        $out = [];

        try {

            foreach ($request->items as $item) {
                //dd($item,  $item["status"]);
                $transaction = MoovMtnTransaction::find($item["ref"]);
                $transaction->status = $item["status"];
                $transaction->save();
                if ($item["status"] == 1) {

                    $user = User::where('ref', $transaction->user_ref)->first();
                    $pricing = Pricing::where('public_key', $transaction->pricing_pk)->first();


                    //dd($item,  $item["status"], $user && $pricing, $user, $pricing);

                    if ($user && $pricing) {

                        $start = Carbon::now();
                        $end = Carbon::now()->addDays($pricing->day_duration);

                        $plan_old = MoovMtnTransaction::where('status', 1)->first();

                        $plan = Plan::create([
                            'user_id' => $user->id,
                            'pricing_id' => $pricing->id,
                            'transaction_key' => $transaction->transaction_key,
                            'start' => $start,
                            'end' => $end,
                            'transaction_id' => $transaction->id,
                            'transaction_class' => MoovMtnTransaction::class,
                        ]);

                        if ($user->sponsor) {
                            $commission = Commission::create([
                                'amount' => (($pricing->gift * $pricing->price) / 100),
                                'plan_id' => $plan->id,
                                'recipient_id' => $user->sponsor->id,
                            ]);
                        }

                        if ($plan_old) {
                            $plan_old->status = 2;
                            $plan_old->save();
                        }
                    }
                }
                array_push($out, new MoovMtnTransactionResource($transaction));
            }


            return ['message' => "MoovMtnTransaction updated", 'data' => $out, 'statusCode' => 200];
        } catch (\Exception $e) {
            return ['message' => json_encode($e->getMessage()), 'data' => sizeof($out) == 0 ? null : $out, 'statusCode' => sizeof($out) == 0 ? 500 : 520, 'error' => true];
        }
    }

    /**
     * Delete moovmtntransaction
     *
     * @param \Ramsey\Uuid\Uuid  $id
     *
     * @return array
     */
    public function delete($id)
    {
        DB::beginTransaction();
        try {
            $moovmtntransaction = MoovMtnTransaction::find($id);

            // Check the moovmtntransaction
            if (!$moovmtntransaction) return ['message' => "No moovmtntransaction with ID $id", 'data' => null, 'statusCode' => 404, 'error' => true];

            $moovmtntransaction->label = $moovmtntransaction->label . "#-#DELETE#";
            $moovmtntransaction->save();

            // Delete the moovmtntransaction
            $moovmtntransaction->delete();

            DB::commit();
            return ['message' => "MoovMtnTransaction deleted", 'data' => true, 'statusCode' => 200];
        } catch (\Exception $e) {
            DB::rollBack();
            return ['message' => $e->getMessage(), 'data' => false, 'statusCode' => 500, 'error' => true];
        }
    }
}

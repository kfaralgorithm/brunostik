<?php

namespace App\Repositories\Secured;

use App\Models\Championat;
use App\Traits\Response;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Resources\ChampionatResource;
use App\Http\Resources\ChampionatCollection;
use App\Interfaces\Secured\ChampionatRepositoryInterface;
use App\Http\Requests\ChampionatCreateOrUpdateRequest;

class ChampionatRepository implements ChampionatRepositoryInterface
{

    /**
     * Get all championats
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function getAll(Request $request)
    {
        try {
            $championats = null;

            if ($request->has('sort_by') && in_array($request->sort_by, ['designation', 'recorded_at', '-designation', '-recorded_at'])) {
                $championats = Championat::orderBy(['designation' => 'label', 'recorded_at' =>  'created_at', '-designation' => 'label', '-recorded_at' =>  'created_at'][$request->sort_by], $request->sort_by[0] == '-' ? 'asc' : 'desc');
            } else {
                $championats = Championat::orderBy('created_at', 'desc');
            }

            /*
             * Filtring on label championat table field
             * designation is the http request query key associated at label
             */
            if ($request->has('designation') && $request->designation) {
                $championats = Championat::where('label', 'LIKE', '%' . trim($request->designation) . '%');
            }
            /*
             * End filtring on label
             */

            /*
             * Filtring on created_at championat table field
             * created_at is datetime
             * recorded_from <= created_at
             * created_at <= recorded_to
             * recorded_at and recorded_from is date Y-m-d
             */

            if ($request->has('recorded_from') && $request->recorded_from) {
                $championats = $championats == null ? Championat::whereDate('created_at', '>=', $request->recorded_from) : $championats->whereDate('created_at', '>=', $request->recorded_from);
            }

            if ($request->has('recorded_to') && $request->recorded_to) {
                $championats = $championats == null ? Championat::whereDate('created_at', '<=', $request->recorded_to) : $championats->whereDate('created_at', '<=', $request->recorded_to);
            }
            /*
             * End filtring on created_at
             */

            $championats = $championats->paginate(25)->appends(Arr::except($request->query(), 'page'));
            return ['message' => "All Championats", 'data' => new ChampionatCollection($championats), 'statusCode' => 200];
        } catch (\Exception $e) {
            return ['message' => $e->getMessage(), 'data' => null, 'statusCode' => 500, 'error' => true];
        }
    }

    /**
     * Get Championat By ID
     *
     * @param \Ramsey\Uuid\Uuid  $id
     *
     * @return array
     */
    public function getById($id)
    {
        try {
            $championat = Championat::find($id);

            // Check the championat
            if (!$championat) return ['message' => "No championat with ID $id", 'data' => null, 'statusCode' => 404, 'error' => true];

            return ['message' => "Championat Detail", 'data' => new ChampionatResource($championat), 'statusCode' => 200];
        } catch (\Exception $e) {
            return ['message' => $e->getMessage(), 'data' => null, 'statusCode' => 500, 'error' => true];
        }
    }

    /**
     * Create | Update championat
     *
     * @param \App\Http\Requests\ChampionatCreateOrUpdateRequest $request
     * @param \Ramsey\Uuid\Uuid  $id
     *
     * @return array
     */
    public function createOrUpdate(ChampionatCreateOrUpdateRequest $request, $id = null)
    {

        DB::beginTransaction();
        try {

            $championat = $id ? Championat::find($id) : new Championat;

            if ($id && !$championat) return ['message' => "No championat with ID $id", 'data' => null, 'statusCode' => 404, 'error' => true];

            $championat->label = trim($request->designation);
            $championat->save();

            DB::commit();

            return ['message' => $id ? "Championat updated" : "Championat created", 'data' => $championat, 'statusCode' => $id ? 200 : 201];
        } catch (\Exception $e) {
            DB::rollBack();
            return ['message' => $e->getMessage(), 'data' => null, 'statusCode' => 500, 'error' => true];
        }
    }

    /**
     * Delete championat
     *
     * @param \Ramsey\Uuid\Uuid  $id
     *
     * @return array
     */
    public function delete($id)
    {
        DB::beginTransaction();
        try {
            $championat = Championat::find($id);

            // Check the championat
            if (!$championat) return ['message' => "No championat with ID $id", 'data' => null, 'statusCode' => 404, 'error' => true];

            $championat->label = $championat->label . "#-#DELETE#";
            $championat->save();

            // Delete the championat
            $championat->delete();

            DB::commit();
            return ['message' => "Championat deleted", 'data' => true, 'statusCode' => 200];
        } catch (\Exception $e) {
            DB::rollBack();
            return ['message' => $e->getMessage(), 'data' => false, 'statusCode' => 500, 'error' => true];
        }
    }
}

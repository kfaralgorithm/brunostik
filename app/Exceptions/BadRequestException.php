<?php

namespace App\Exceptions;

use Exception;
use App\Traits\Response;

class BadRequestException extends Exception
{
    use Response;
    public function render($request)
    {
        return response()->json(["error" => true, "message" => $this->getMessage()]);
    }
}

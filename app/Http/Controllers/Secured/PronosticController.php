<?php

namespace App\Http\Controllers\Secured;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interfaces\Secured\PronosticRepositoryInterface;
use App\Http\Requests\PronosticCreateOrUpdateRequest;

class PronosticController extends Controller
{
    protected $PronosticRepository;

    public function __construct(PronosticRepositoryInterface $PronosticRepository)
    {
        $this->PronosticRepository = $PronosticRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return $this->response($this->PronosticRepository->getAll($request));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PronosticCreateOrUpdateRequest $request)
    {
        return $this->response($this->PronosticRepository->createOrUpdate($request));
    }

    /**
     * Display the specified resource.
     *
     * @param  \ramsey\uuid\uuid  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->response($this->PronosticRepository->getById($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \ramsey\uuid\uuid  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PronosticCreateOrUpdateRequest $request, $id)
    {
        return $this->response($this->PronosticRepository->createOrUpdate($request, $id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \ramsey\uuid\uuid  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $this->response($this->PronosticRepository->delete($id));
    }
}

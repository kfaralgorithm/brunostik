<?php

namespace App\Http\Controllers\Opened\Auth\V2;

use Carbon\Carbon;
use App\Models\Plan;
use App\Models\User;
use App\Models\Commission;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\PlanResource;
use App\Http\Requests\OpenedLoginRequest;
use App\Http\Resources\UserLoginResource;

class LoginController extends Controller
{
    static public $err_class_code = "LO2000";

    public function login(OpenedLoginRequest $request)
    {
        /*
         * ERROR CODE
         * LO2000-XS-FIC = FAILDED INVALID CREDENTIAL
         * LO2000-XS-ISECI = INTERNAL SERVER ERROR INVALID CREDENTIAL
         * LO2000-XS-ISE = INTERNAL SERVER ERROR
            */
        $err_function_code = self::$err_class_code . '-' . 'XS' . '-';


        //try {
        $users = User::where('email', strtolower(trim($request->emus)))->orWhere('username', trim($request->emus))->get();

        //dd($request->emus, strtolower(trim($request->emus)), $users->count());

        if ($users->count() != 1) {
            return $this->response(['message' => 'Invalid Credentials #' . $err_function_code . ($users->count() == 0 ? 'FIC' : 'ISECI') . '', 'error' => true, 'statusCode' => 401]);
        }

        $user = $users->first();

        if (!auth()->attempt(['email' => $user->email, 'password' => $request->scode])) {
            return $this->response(['message' => 'Invalid Credentials #' . $err_function_code . 'FIC.', 'error' => true, 'statusCode' => 401]);
        }

        //dd($user);

        $accessToken = auth()->user()->createToken($user->token_id)->accessToken;


        $plan = Plan::where('user_id', auth()->user()->id)->where('start', '<=', Carbon::now())->where('end', '>=', Carbon::now())->first();


        $user = auth()->user();
        $commission = [
            'done' => Commission::where('recipient_id', $user->id)->where('status', 1)->sum('amount'),
            'pending' =>  Commission::where('recipient_id', $user->id)->where('status', 0)->sum('amount'),
            'cancel' => Commission::where('recipient_id', $user->id)->where('status', -1)->sum('amount'),
            'total' => Commission::where('recipient_id', $user->id)->sum('amount'),
        ];

        $substribtion = Plan::where('user_id', $user->id)->count();

        $downline = User::where('sponsor_id', $user->id)->count();

        return $this->response([
            'message' => 'User logged successfully.',
            'data' =>  [
                'info' => new UserLoginResource($user, $accessToken),
                'subscription' => [
                    'subscribed' => ($plan ? true : false),
                    'item' => ($plan ? new PlanResource($plan) : null)
                ],
                'statistic' => compact("commission", "substribtion", "downline")
            ],
            'statusCode' => 200
        ]);
        /*} catch (\Exception $ex) {
            return $this->response(['message' => 'User registered failed #' . $err_function_code . 'ISE.', 'error' => true, 'statusCode' => 500]);
        }*/
    }
}

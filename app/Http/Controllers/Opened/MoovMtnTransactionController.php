<?php

namespace App\Http\Controllers\Opened;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interfaces\Opened\MoovMtnTransactionRepositoryInterface;
use App\Http\Requests\MoovMtnTransactionCreateRequest;

class MoovMtnTransactionController extends Controller
{
    protected $moovmtntransactionRepository;

    public function __construct(MoovMtnTransactionRepositoryInterface $moovmtntransactionRepository)
    {
        $this->moovmtntransactionRepository = $moovmtntransactionRepository;
    }


    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return $this->response($this->moovmtntransactionRepository->getAll($request));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MoovMtnTransactionCreateRequest $request)
    {
        return $this->response($this->moovmtntransactionRepository->create($request));
    }
}

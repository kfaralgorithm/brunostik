<?php

namespace App\Http\Requests;

use App\Traits\Response;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class PricingCreateOrUpdateRequest extends FormRequest
{
    use Response;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (request()->route('pricing') && \Ramsey\Uuid\Uuid::isValid(request()->route('pricing'))) {
            return [
                'designation' => 'min:3|max:250',
                'percent' => 'numeric|between:1,99',
                'day' => 'integer',
                'month' => 'integer',
                'price' => 'numeric',
            ];
        }
        return [
            'designation' => 'required|min:3|max:250',
            'percent' => 'required|numeric|between:1,99',
            'day' => 'required|integer',
            'month' => 'required|integer',
            'price' => 'required|numeric',
        ];
    }


    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(
            $this->response(['message' => $validator->errors(), 'error' => true, 'statusCode' => 400])
        );
    }
}

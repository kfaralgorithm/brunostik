<?php

namespace App\Http\Requests;

use App\Traits\Response;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class BetCreateOrUpdateRequest extends FormRequest
{
    use Response;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //dd(request()->route('bet'), request()->bet);

        if (request()->route('bet') && \Ramsey\Uuid\Uuid::isValid(request()->route('bet'))) {
            return [
                'status' => 'numeric|in:1,0,1',
                'key' => 'min:3|max:100',
                'cote' => 'numeric',
                'available_at' => 'date',
                'label' => 'min:3|max:250',
            ];
        }
        return [
            'key' => 'required|min:3|max:100',
            'cote' => 'required|numeric',
            'available_at' => 'required|date',
            'label' => 'required|min:3|max:250',
        ];

        /*return [
            'key' => 'required|min:3|max:100',
            'cote' => 'required|numeric',
            'available_at' => 'required|date',
        ];*/
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(
            $this->response(['message' => $validator->errors(), 'error' => true, 'statusCode' => 400])
        );
    }
}

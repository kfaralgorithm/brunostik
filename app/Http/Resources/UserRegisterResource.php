<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserRegisterResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'ref' => $this->id,
            'firstname' => $this->forname,
            'lastname' => $this->surname,
            'username' => $this->username,
            'email' => $this->email,
            'phone' => $this->phone,
            'personrefkey' => $this->ref,
            'sponsref' => [
                'firstname' => $this->sponsor->forname,
                'lastname' => $this->sponsor->surname,
                'personrefkey' => $this->sponsor->ref,
            ],
        ];
    }
}

<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class CommissionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        return [
            'ref' => $this->id,
            'status' => $this->status,
            'amount' => $this->amount,
            'recipient' =>  [
                'firstname' => $this->recipient->forname,
                'lastname' => $this->recipient->surname,
                'username' => $this->recipient->username,
                'email' => $this->recipient->email,
            ],
            'transaction' => [
                'ref' => $this->id,
                'status' => $this->plan->status,
                'start' => $this->plan->start,
                'end' => $this->plan->end,
                'author' => [
                    'firstname' => $this->plan->author->forname,
                    'lastname' => $this->plan->author->surname,
                    'username' => $this->plan->author->username,
                    'email' => $this->plan->author->email,
                ]

            ],
            'recorded_at' => $this->created_at ? Carbon::createFromFormat(datetime_format_db(), $this->created_at)->format(datetime_format_system()) : null,
            'modified_at' => $this->updated_at ? Carbon::createFromFormat(datetime_format_db(), $this->updated_at)->format(datetime_format_system()) : null,
            'removed_at' => $this->deleted_at ? Carbon::createFromFormat(datetime_format_db(), $this->deleted_at)->format(datetime_format_system()) : null,
            'href' => [
                'link' => route('admin.commissions.show', ['commission' => $this->id]),
            ],
        ];
    }
}
